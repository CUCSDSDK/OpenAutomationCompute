package com.cloupia.feature.compute.accounts.inventory;

import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.JSON;
import com.cloupia.feature.compute.accounts.StorageAccount;
import com.cloupia.feature.compute.accounts.util.FooAccountPersistenceUtil;
import com.cloupia.lib.connector.InventoryContext;
import com.cloupia.lib.connector.InventoryEventListener;
import com.cloupia.lib.connector.account.AbstractInfraAccount;
import com.cloupia.lib.connector.account.AccountTypeEntry;
import com.cloupia.lib.connector.account.AccountUtil;
import com.cloupia.lib.connector.account.PhysicalAccountManager;
import com.cloupia.lib.connector.account.PhysicalConnectivityStatus;
import com.cloupia.lib.connector.account.PhysicalInfraAccount;

public class StorageInventoryListener implements InventoryEventListener {
	private static Logger logger = Logger.getLogger(StorageInventoryListener.class);

	@Override
	public void afterInventoryDone(String accountName, InventoryContext context)
			throws Exception {
		// TODO Auto-generated method stub
		logger.info("Call in StorageInventoryListener afterInventoryDone");
		
		FooAccountPersistenceUtil.persistCollectedInventory(accountName);
		
		AccountTypeEntry entry = PhysicalAccountManager.getInstance().getAccountTypeEntryByName(accountName);
		PhysicalConnectivityStatus connectivityStatus =null;
		if(entry != null)
		{
			connectivityStatus = entry.getTestConnectionHandler().testConnection(accountName);
		}
		
		StorageAccount acc = getFooCredential(accountName);
		
		if(acc != null && connectivityStatus != null)
		{
			
			
			logger.info("Inventory collected successfully");
		}


	}

	@Override
	public void beforeInventoryStart(String accountName, InventoryContext arg1)
			throws Exception {
		logger.info("Call in StorageInventoryListener beforeInventoryStart");
		
	}
	
	private static StorageAccount getFooCredential(String accountName) throws Exception{
		PhysicalInfraAccount acc = AccountUtil.getAccountByName(accountName);
		String json = acc.getCredential();
		AbstractInfraAccount specificAcc  =  (AbstractInfraAccount) JSON.jsonToJavaObject(json, StorageAccount.class);
		specificAcc.setAccount(acc);
		
		return (StorageAccount) specificAcc;
		
	}

}
