package com.cloupia.feature.compute.accounts.inventory;

import java.util.ArrayList;
import java.util.List;

import com.cloupia.feature.compute.constants.ComputeConstants;
import com.cloupia.model.cIM.ConvergedStackComponentDetail;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.service.cIM.inframgr.reports.contextresolve.ConvergedStackComponentBuilderIf;

public class ComputeAccountConvergedStackBuilder implements ConvergedStackComponentBuilderIf{

	@Override
	public ConvergedStackComponentDetail buildConvergedStackComponent(
			String arg0) throws Exception {
		ConvergedStackComponentDetail detail = new ConvergedStackComponentDetail();
		detail.setModel("Dummy Model");
		detail.setOsVersion("1.0");
		detail.setVendorLogoUrl("/app/uploads/openauto/dummy_logo.png");
		detail.setIconUrl("/app/uploads/openauto/dummy_logo.png");
		detail.setMgmtIPAddr("172.29.109.219");
		detail.setStatus("OK");
		detail.setVendorName("Dummy");
		detail.setContextType(ReportContextRegistry.getInstance().getContextByName(ComputeConstants.INFRA_ACCOUNT_TYPE).getType());
		/*LAYER_TYPE_PHY_COMPUTE = 1;
	      LAYER_TYPE_PHY_NETWORK = 2;
	      LAYER_TYPE_PHY_STORAGE = 3;*/
		detail.setLayerType(1);
		detail.setComponentSummaryList(getSummaryReports());
		return detail;
	}

	private List<String> getSummaryReports()
			throws Exception {
		
		 List<String> rpSummaryList = new ArrayList<String>();
		 rpSummaryList.add("test");
		 rpSummaryList.add("test2");
				return rpSummaryList;
	
	}
}
