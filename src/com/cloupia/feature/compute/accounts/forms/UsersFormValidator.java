/**
 * Copyright (c) 2009-2012 Cloupia, Inc. All rights reserved.
 */
package com.cloupia.feature.compute.accounts.forms;


import com.cloupia.service.cIM.inframgr.forms.wizard.FormManagedList;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormManagedListValidatorIf;
import com.cloupia.service.cIM.inframgr.forms.wizard.Page;
import com.cloupia.service.cIM.inframgr.forms.wizard.WizardSession;

public class UsersFormValidator implements FormManagedListValidatorIf<UsersForm>
{
    @Override
    public String getValidatorName()
    {
        return null;
    }

    @Override
    public void verifyAdd(FormManagedList<UsersForm> list, UsersForm object, Page page,
            WizardSession session) throws Exception
    {
    	
    	if(null!=object){  
    		if ((object.getFirstName() == null) && (object.getFirstName().isEmpty()))
            {
            	throw new Exception( 
                        "First name cannot be null or empty.");
            }
    		if ((object.getLastName() == null) && (object.getLastName().isEmpty()))
            {
            	throw new Exception( 
                        "Last name cannot be null or empty.");
            } 
    		if ((object.getEmailAddress() == null) && (object.getEmailAddress().isEmpty()))
            {
            	throw new Exception( 
                        "Email cannot be null or empty.");
            } 
    	} 
        for (UsersForm n : list.getList())
        {
            
            if ((object.getEmailAddress() != null) && (n.getEmailAddress() != null)
                    && n.getEmailAddress().equals(object.getEmailAddress()))
            {
                throw new Exception("Duplicate Email: " + object.getEmailAddress());
            } 
        }
             

      
    }

    @Override
    public void verifyDelete(FormManagedList<UsersForm> list, UsersForm object, Page page,
            WizardSession session) throws Exception
    {
        if (list.getList().size()==1)
        {
            throw new Exception("Can not delete the user.");
        }
    }

    @Override
    public void verifyUpdate(FormManagedList<UsersForm> list, UsersForm object, Page page,
            WizardSession session) throws Exception
    {
    	
    	for (UsersForm n : list.getList())
        {
            
            
            if ((object.getEmailAddress() != null) && (n.getEmailAddress() != null)
                    && n.getEmailAddress().equals(object.getEmailAddress()))
            {
                throw new Exception("Duplicate Email: " + object.getEmailAddress());
            } 
        }
            	
    	
        
    }
}
