package com.cloupia.feature.compute.accounts.forms;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.cloupia.lib.easyui.annotations.NonReportField;
import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.FormFieldDefinition;
import com.cloupia.service.cIM.inframgr.forms.wizard.AbstractFormManagedObject;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormField;
import com.cloupia.service.cIM.inframgr.forms.wizard.GenericFormManagedObjectType;
@XmlRootElement(name="User")
@SuppressWarnings("serial")
@PersistenceCapable(detachable = "true", table = "compute_account_users")
public class UsersForm extends AbstractFormManagedObject implements GenericFormManagedObjectType{
	
	
	@NonReportField
	@XmlTransient
	@Persistent(sequence = "fielddefidseq", valueStrategy = IdGeneratorStrategy.INCREMENT)
	public long objId = -1;
	
	
	@FormField(label = "First Name", help = "", type = FormFieldDefinition.FIELD_TYPE_TEXT)
    @ReportField(label = "First Name")
    private String firstName;

    @FormField(label = "Last Name", help = "", type = FormFieldDefinition.FIELD_TYPE_TEXT)
    @ReportField(label = "Last Name")
    private String lastName;

    @FormField(label = "Email Address", help = "", type = FormFieldDefinition.FIELD_TYPE_TEXT)
    @ReportField(label = "Email Address")
    private String emailAddress;

   public UsersForm(){
	   
   };
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	@Override
	public String toString() {
		return "UsersForm [firstName=" + firstName + ", lastName=" + lastName + ", emailAddress=" + emailAddress + "]";
	}
	@Override
	public long getObjId() {
		return objId;
	}

    
}
