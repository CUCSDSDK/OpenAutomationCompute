package com.cloupia.feature.compute.constants;


public class ComputeConstants {

	public static final String DEVICE_LIST_PROVIDER = "foo_DeviceListProvider";
	public static final String NEXUS_DEVICE_TABLE = "foo_DeviceTable";
	
	public static final String TEMP_EMAIL_ADDRESSES = "foo_email_address_list";
    public static final String NEXUS_DEVICE_LIST = "foo_nexus_device_list";
   
    public static final String EMAIL_TASK_OUTPUT_NAME = "Datacenter Email Addresses";
    public static final String EMAIL_TASK_OUTPUT_TYPE = "e-mail-as-string";
    
	public static final String NEXUS_DEVICES_LOV_PROVIDER = "NetworkDeviceList";

	public static final String FOO_HELLO_WORLD_NAME = "compute_name_from_other_tasks";
	
	//this is the unique integer i'm giving for my dummy collector, it's a good idea
	//to use some large number past 1000 so you avoid any potential collisions
	public static final int DUMMY_ACCOUNT_TYPE = 7000;
	
	//some dummy strings used to represent inventory items
	public static final String INTERFACES = "interfaces";
	public static final String PORTS = "ports";
	
	public static final String DUMMY_INVENTORY_COLLECTOR_NAME = "Dummy_Inventory_Collector";
	
	public static final String DUMMY_VLAN_RESOURCE_TYPE = "compute.vlan.per.group.usage";
	public static final String DUMMY_VLAN_RESOURCE_DESC = "Max Dummy VLANs per group";
	
	public static final int DUMMY_MENU_1 = 23001;
	
	public static final String DUMMY_CONTEXT_ONE = "compute.compute.dummy.context.one";
	public static final String DUMMY_CONTEXT_ONE_LABEL = "Dummy Context One";
	
	public static final String INFRA_ACCOUNT_LABEL = "Compute Account";
	public static final String INFRA_ACCOUNT_TYPE = "Compute Account";
	
	public static final String FOO_ACCOUNT_DRILLDOWN_NAME = "compute.compute.account.sample.child.drilldown.report";
	public static final String FOO_ACCOUNT_DRILLDOWN_LABEL = "Compute Account Drilldown Sample";
	
	public static final String COMPUTE_ACCOUNT_DRILLDOWN_NAME = "compute.compute.drilldown.report";
	public static final String COMPUTE_ACCOUNT_DRILLDOWN_LABEL = "Drill Down";
	
	public static final String INVENTORY_ROOT = "compute.inventory.root";
	
	public static final String DRILL_DOWN_CHILD_REPORT_NAME = "compute.drilldown.child.report";
	public static final String DRILL_DOWN_CHILD_REPORT_LABEL = "Drill Down Child";
	
	public static final String REST_API_FOLDER_NAME = "Open Automation REST API";
	public static final String COMPUTE_ACCOUNT_NAME_OUTPUT = "OUTPUT_ACCOUNT_NAME";
	public static final String COMPUTE_ACCOUNT_IP_OUTPUT = "OUTPUT_ACCOUNT_IP";
	public static final String COMPUTE_ACCOUNT_STATUS_OUTPUT = "OUTPUT_ACCOUNT_STATUS";
	public static final String COMPUTE_ACCOUNT_USEERS_SOUTPUT = "OUTPUT_ACCOUNT_USERS";
	
	public static final String USERS_LIST_TYPE=  "compute_usersListType";

}
