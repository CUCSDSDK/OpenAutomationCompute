package com.cloupia.feature.compute;


import org.apache.log4j.Logger;

import com.cloupia.feature.compute.accounts.DummyAccount;
import com.cloupia.service.cIM.tree.MoOpType;
import com.cloupia.feature.compute.accounts.ComputeAccount;
import com.cloupia.feature.compute.accounts.ComputeAccountSampleReport;
import com.cloupia.feature.compute.accounts.handler.ComputeAccountConnectionHandler;
import com.cloupia.feature.compute.accounts.inventory.ComputeAccountConvergedStackBuilder;
import com.cloupia.feature.compute.accounts.inventory.ComputeInventoryItemHandler;
import com.cloupia.feature.compute.accounts.inventory.ComputeInventoryListener;
import com.cloupia.feature.compute.constants.ComputeConstants;
import com.cloupia.feature.compute.drilldownreports.ComputeAccountSampleDrillDownReport;
import com.cloupia.feature.compute.dummyOne.reports.DummyOneSampleReport;
import com.cloupia.feature.compute.lovs.SimpleLovProvider;
import com.cloupia.feature.compute.lovs.SimpleTabularProvider;
import com.cloupia.feature.compute.menuProvider.DummyMenuProvider;
import com.cloupia.feature.compute.resourceComputer.DummyVLANResourceComputer;
import com.cloupia.feature.compute.scheduledTasks.DummyScheduleTask;
import com.cloupia.feature.compute.tasks.CreateGroupTask;
import com.cloupia.feature.compute.tasks.DisableSNMPNexusTask;
import com.cloupia.feature.compute.tasks.EmailDatacentersTask;
import com.cloupia.feature.compute.tasks.EnableSNMPNexusTask;
import com.cloupia.feature.compute.tasks.HelloWorldTask;
import com.cloupia.feature.compute.tasks.RollbackHelloWorldTask;
import com.cloupia.feature.compute.triggers.MonitorDummyDeviceStatusParam;
import com.cloupia.feature.compute.triggers.MonitorDummyDeviceType;
import com.cloupia.feature.compute.api.tasks.ComputeAccountCreateFormManagedTask;
import com.cloupia.feature.compute.api.tasks.ComputeAccountCreateAPITask;
import com.cloupia.feature.compute.api.tasks.ComputeAccountDeleteAPITask;
import com.cloupia.feature.compute.api.tasks.ComputeAccountUpdateAPITask;
import com.cloupia.feature.compute.api.config.CreateComputeAccountWithUsersConfig;
import com.cloupia.feature.compute.api.config.ComputeAccountConfig;
import com.cloupia.feature.compute.api.config.ComputeAccountCreateConfig;
import com.cloupia.feature.compute.api.config.ComputeAccountDeleteConfig;
import com.cloupia.feature.compute.api.config.ComputeAccountListnerConfig;
import com.cloupia.feature.compute.api.config.ComputeAccountUpdateConfig;
import com.cloupia.feature.compute.api.listner.ComputeResourceAPIListner;
import com.cloupia.feature.compute.api.reports.ComputeAccountAPIsReport;
import com.cloupia.feature.compute.api.reports.ComputeAccountListenerAPIsReport;
import com.cloupia.lib.connector.ConfigItemDef;
import com.cloupia.lib.connector.account.AccountTypeEntry;
import com.cloupia.lib.connector.account.PhysicalAccountTypeManager;
import com.cloupia.model.cIM.InfraAccountTypes;
import com.cloupia.model.cIM.ReportContextRegistry;
import com.cloupia.service.cIM.inframgr.AbstractCloupiaModule;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.CustomFeatureRegistry;
import com.cloupia.service.cIM.inframgr.collector.controller.CollectorFactory;
import com.cloupia.service.cIM.inframgr.reports.simplified.CloupiaReport;
import com.cloupia.service.cIM.inframgr.thresholdmonitor.MonitoringTrigger;
import com.cloupia.service.cIM.inframgr.thresholdmonitor.MonitoringTriggerUtil;
import com.cloupia.service.cIM.tree.MoParser;
import com.cloupia.service.cIM.tree.MoPointer;
import com.cloupia.service.cIM.tree.WFTaskRestAdaptor;

public class ComputeModule extends AbstractCloupiaModule {

	private static Logger logger = Logger.getLogger(ComputeModule.class);

	@Override
	public AbstractTask[] getTasks() {
		AbstractTask task1 = new CreateGroupTask();
		AbstractTask task2 = new EmailDatacentersTask();
		AbstractTask task3 = new HelloWorldTask();
		AbstractTask task4 = new EnableSNMPNexusTask();
		AbstractTask task5 = new DisableSNMPNexusTask();
		AbstractTask task6 = new RollbackHelloWorldTask();
		AbstractTask task7 = new ComputeAccountCreateAPITask();
		AbstractTask task8 = new ComputeAccountUpdateAPITask();
		AbstractTask task9 = new ComputeAccountDeleteAPITask();
		AbstractTask task10 = new ComputeAccountCreateFormManagedTask();
		AbstractTask[] tasks = new AbstractTask[10];
		tasks[0] = task1;
		tasks[1] = task2;
		tasks[2] = task3;
		tasks[3] = task4;
		tasks[4] = task5;
		tasks[5] = task6;
		tasks[6] = task7;
		tasks[7] = task8;
		tasks[8] = task9;
		tasks[9] = task10;
		
		return tasks;
	}

	/**
	 * Getcollectors is not required for the creating new account type.
	 * getCollectors method be deprecated for the new account type.
	 * 
	 */
	@Override
	public CollectorFactory[] getCollectors() {
		return null;
	}

	@Override
	public CloupiaReport[] getReports() {
		// this is where you register all your top level reports, i'm only
		// registering the report
		// extending genericinfraaccountreport because all my other reports are
		// actually drilldown
		// reports of that report

		CloupiaReport[] reports = new CloupiaReport[5];
		reports[0] = new DummyOneSampleReport();
		reports[1] = new ComputeAccountSampleReport();
		ComputeAccountSampleDrillDownReport drilReport = new ComputeAccountSampleDrillDownReport(
				ComputeConstants.COMPUTE_ACCOUNT_DRILLDOWN_NAME, ComputeConstants.COMPUTE_ACCOUNT_DRILLDOWN_LABEL, DummyAccount.class);
		
		reports[2] = drilReport;
		reports[3] = new ComputeAccountAPIsReport();
		reports[4] = new ComputeAccountListenerAPIsReport();
		return reports;
	}

	@Override
	public void onStart(CustomFeatureRegistry cfr) {
		// this is where you would register stuff like scheduled tasks or
		// resource computers

		// when registering new resource types to limit, you need to provide an
		// id to uniquely identify the resource,
		// a description of how that resource is computed, and an instance of
		// the computer itself
		this.registerResourceLimiter(ComputeConstants.DUMMY_VLAN_RESOURCE_TYPE,
				ComputeConstants.DUMMY_VLAN_RESOURCE_DESC, new DummyVLANResourceComputer());

		try {
			// this is where you should register LOV providers for use in
			// SimpleDummyAction
			cfr.registerLovProviders(SimpleLovProvider.SIMPLE_LOV_PROVIDER, new SimpleLovProvider());
			// you need to provide a unique id for this tabular provider, along
			// with the implementation class, and the
			// index of the selection and display columns, for most cases, you
			// can blindly enter 0
			cfr.registerTabularField(SimpleTabularProvider.SIMPLE_TABULAR_PROVIDER, SimpleTabularProvider.class, "0",
					"0");
			// this is where you should add your schedule tasks
			addScheduleTask(new DummyScheduleTask());

			// registering new report context for use in my dummy menu, good
			// rule of thumb, always register your contexts
			// as early as possible, this way you won't run into any cases where
			// the context does not exist yet and causes
			// an issue elsewhere in the code!
			ReportContextRegistry.getInstance().register(ComputeConstants.DUMMY_CONTEXT_ONE,
					ComputeConstants.DUMMY_CONTEXT_ONE_LABEL);

			// StorageAccount
			ReportContextRegistry.getInstance().register(ComputeConstants.INFRA_ACCOUNT_TYPE,
					ComputeConstants.INFRA_ACCOUNT_LABEL);

			// Foo Drill down REport
			ReportContextRegistry.getInstance().register(ComputeConstants.FOO_ACCOUNT_DRILLDOWN_NAME,
					ComputeConstants.FOO_ACCOUNT_DRILLDOWN_LABEL);

			// register the left hand menu provider for the menu item i'm
			// introducing
			DummyMenuProvider menuProvider = new DummyMenuProvider();

			// adding new monitoring trigger, note, these new trigger components
			// utilize the dummy context one i've just registered
			// you have to make sure to register contexts before you execute
			// this code, otherwise it won't work
			MonitoringTrigger monTrigger = new MonitoringTrigger(new MonitorDummyDeviceType(),
					new MonitorDummyDeviceStatusParam());
			MonitoringTriggerUtil.register(monTrigger);
			menuProvider.registerWithProvider();
			// support for new Account Type
			createAccountType();
		} catch (Exception e) {
			logger.error("Foo Module error registering components.", e);
		}

	}

	/**
	 * Creating New Account Type
	 */
	private void createAccountType() {
		AccountTypeEntry entry = new AccountTypeEntry();
		// This is mandatory, hold the information for device credential details
		entry.setCredentialClass(ComputeAccount.class);

		// This is mandatory, type of the Account will be shown in GUI as drill
		// down box
		entry.setAccountType(ComputeConstants.INFRA_ACCOUNT_TYPE);

		// This is mandatory, label of the Account
		entry.setAccountLabel(ComputeConstants.INFRA_ACCOUNT_LABEL);

		// This is mandatory, specify the category of the account type ie.,
		// Network / Storage / //Compute
		entry.setCategory(InfraAccountTypes.CAT_COMPUTING);

		// This is mandatory
		entry.setContextType(
				ReportContextRegistry.getInstance().getContextByName(ComputeConstants.INFRA_ACCOUNT_TYPE).getType());

		// This is mandatory, on which accounts either physical or virtual
		// account , new account //type belong to.
		entry.setAccountClass(AccountTypeEntry.PHYSICAL_ACCOUNT);
		// Optional , prefix of the task
		entry.setInventoryTaskPrefix("Open Automation Inventory Task");

		// Optional. Group inventory system tasks under this folder.
		// By default it is grouped under General Tasks
		entry.setWorkflowTaskCategory("Compute Tasks");
		// Optional , collect the inventory frequency, whenever required you can
		// change the
		// inventory collection frequency, in mins.
		entry.setInventoryFrequencyInMins(15);
		// This is mandatory,under which pod type , the new account type is
		// applicable.
		entry.setPodTypes(new String[] { "FlexPod" });

		// This is optional, dependents on the need of session for collecting
		// the inventory
		// entry.setConnectorSessionFactory(new FooSessionFactory());

		// This is mandatory, to test the connectivity of the new account. The
		// Handler should be of type PhysicalConnectivityTestHandler.
		entry.setTestConnectionHandler(new ComputeAccountConnectionHandler());
		// This is mandatory, we can implement inventory listener according to
		// the account Type , collect the inventory details.
		entry.setInventoryListener(new ComputeInventoryListener());

		// This is mandatory , to show in the converged stack view
		entry.setConvergedStackComponentBuilder(new ComputeAccountConvergedStackBuilder());

		// This is required to show up the details of the stack view in the GUI
		// entry.setStackViewItemProvider(new FooStackViewProvider());

		// This is required credential.If the Credential Policy support is
		// required for this Account type then this is mandatory, can implement
		// credential check against the policyname.
		// entry.setCredentialParser(new StorageAccountCredentialParser());
		try {

			// Adding inventory root
			registerInventoryObjects(entry);
			PhysicalAccountTypeManager.getInstance().addNewAccountType(entry);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void registerInventoryObjects(AccountTypeEntry fooRecoverPointAccountEntry) {
		ConfigItemDef fooRecoverPointStateInfo = fooRecoverPointAccountEntry
				.createInventoryRoot(ComputeConstants.INVENTORY_ROOT, ComputeInventoryItemHandler.class);
	}
	/*
	 * InstallMoPointer is required to register REST API in the UCSD platform.
	 * 
	 */
	@Override
	public void installMoPointer(MoParser parser) {
    	try {		    		
    		
    		/*
    		 * Rest Adaptor is used to handle the CRUD operations for the Resource. 
    		 * we can extend the adaptor functionality by inheriting the WFTaskRestAdaptor. 
    		 * We can override the methods createResource, updateResource, deleteResource and query according to the need
    		 */
    		WFTaskRestAdaptor restAdaptor = new WFTaskRestAdaptor();
    		/*
    		 * MoPointer is the placeholder to register the REST APIs.
    		 * @param0 is to define the resource name. mandatory field.
    		 * @param1 is to define for  the ResourceURL.mandatory field.
    		 * @param2 is restAdaptor. mandatory field.
    		 * @param3 is the resource config class. mandatory field.
      		 * If we don't want to READ Operation for the api have to use below constructor.
    		 * MoPointer(String name, String path, MoResourceListener moListener, Class moModel, boolean isMoPersistent ,boolean isReadAPISupported)
    		 * mopointer IS successfully registered API then only we can see the API in REST API Browser.
    		 */
    		 
            MoPointer p = new MoPointer("ComputeAccount", "ComputeAccount", restAdaptor, ComputeAccountConfig.class);  
            /*
             * createOARestOperation is mandatory method to register REST APIs with custom operation names via Open Automation connector.
             * @param0 defines Operation name. mandatory field.
             * @Param1 is resource Handler name. mandatory field.Using this handler name we are handling for the REST API Operation.
             * @param2 is resource config class. mandatory field.For a particular handler operation config is required.
             */
            p.createOARestOperation("CREATE_COMPUTE_ACCOUNT", ComputeAccountCreateConfig.HANDLER_NAME, ComputeAccountCreateConfig.class);
            p.createOARestOperation("DELETE_COMPUTE_ACCOUNT", ComputeAccountDeleteConfig.HANDLER_NAME, ComputeAccountDeleteConfig.class);
            p.createOARestOperation("UPDATE_COMPUTE_ACCOUNT", ComputeAccountUpdateConfig.MODIFY_HANDLER_NAME, ComputeAccountUpdateConfig.class);  
            
            /*
             * addWFCreateTaskPointer is mandatory method to register REST APIs with CREATE operation via Open Automation connector.
             * addWFUpdateTaskPointer is mandatory method to register REST APIs with UPDATE operation via open Automation Connector.
             * addWFDeleteTaskPointer is mandatory method to register REST APIs with DELETE operation via Open Automation Connector.
             * @Param0 is resource Handler name. mandatory field.Using this handler name we are handling for the REST API Operation.
             * @param1 is resource config class. mandatory field.For a particular handler operation config is required.
             */
            p.addWFCreateTaskPointer(CreateComputeAccountWithUsersConfig.HANDLER_NAME, CreateComputeAccountWithUsersConfig.class);
	           
            /*
             * catagory is for the rest API browser folder structor.
             */
            p.setCategory(ComputeConstants.REST_API_FOLDER_NAME);
            //pointer.setCategory(ComputeConstants.REST_API_FOLDER_NAME);
            /*
             * Registered REST APIs intimated to the framework through the MoParser.It is mandatory to load any REST APis in in the framework.
             */
            parser.addMoPointer(p);
            
    	} catch (Exception e) {
    		logger.error("Error installing OA Compute Adaptor APIs " + e);
    	}
    	
    	try {		    		
    		/*
    		 * Rest Listener is used to handle the CRUD operations for the Resource. 
    		 * we can extend the Listener functionality by inheriting the AbstractResourceHandler. 
    		 * We need to override the methods createResource, updateResource, deleteResource and query according to the needed
    		 */
    	    ComputeResourceAPIListner nPolcyList = new ComputeResourceAPIListner();
    		MoPointer p = new MoPointer("ComputeResource", "ComputeResource", nPolcyList, ComputeAccountListnerConfig.class); 
    		/*
    		 * setSupportedOps is mandatory method to register Operations for the respective Resource.
    		 * we use to take MoOpType.CREATE,MoOpType.UPDATE and MoOpType.DELETE for CRUD operations and MoOpType.READ is default Operation name.
    		 * If we want to specify only single Operation like MoOpType.CREATE we have to define only MoOpType.CREATE in the below 'setSupportedOps' method.
    		 */
    		p.setSupportedOps(MoOpType.CREATE,MoOpType.UPDATE,MoOpType.DELETE);
    		
    		p.setCategory(ComputeConstants.REST_API_FOLDER_NAME);
    		parser.addMoPointer(p);
    		} catch (Exception e) {
    				logger.error("Error installing OA Compute Listener APIs " + e);
    			}
    }

}
