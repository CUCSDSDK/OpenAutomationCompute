package com.cloupia.feature.compute.api.listner;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.compute.api.config.ComputeAccountListnerConfig;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.tree.AbstractResourceHandler;
import com.cloupia.service.cIM.tree.DefaultMoProvider;
import com.cloupia.service.cIM.tree.ManagedObject;
import com.cloupia.service.cIM.tree.MoContext;
import com.cloupia.service.cIM.tree.MoResourceIf;
import com.cloupia.service.cIM.tree.ResourceOperationResponse;



public class ComputeResourceAPIListner extends AbstractResourceHandler{
	
	private static Logger       logger       = Logger.getLogger(ComputeResourceAPIListner.class);
	 public ManagedObject query(MoContext context) throws Exception
	    {
		 
	        return new DefaultMoProvider().query(context);
	        
	    }
	
	 /* (non-Javadoc)
     * @see com.cloupia.service.cIM.tree.MoResourceListener#createResource(java.lang.Object)
     */
    @Override
    public ResourceOperationResponse createResource(MoResourceIf object) throws Exception
    {
    	ResourceOperationResponse response = new ResourceOperationResponse();
		response.setOperationStatus(0);
		ComputeAccountListnerConfig oldDef = (ComputeAccountListnerConfig) object;
		String accountName = oldDef.getAccountName();
		try {
			
			ObjStore<ComputeAccountListnerConfig> store = ObjStoreHelper.getStore(ComputeAccountListnerConfig.class);
			List<ComputeAccountListnerConfig> list = store.query("accountName == '"+accountName+"'");
			if(list == null || list.isEmpty()){
				store.insert(oldDef);
				response.setResponse("Created successfully");
			}else{
				throw new Exception("account "+accountName+" already exists.");
			}
		} catch (Exception e) {
			response.setOperationStatus(4);
			response.setErrorMessage(e.getMessage());
			logger.info("failed to create compute Resource Account", e);
		}
		return response;
    }

    /* (non-Javadoc)
     * @see com.cloupia.service.cIM.tree.MoResourceListener#deleteResource(java.lang.Object, java.lang.Object)
     */
    @Override
    public ResourceOperationResponse deleteResource(MoResourceIf resource) throws Exception
    {
    	ResourceOperationResponse response = new ResourceOperationResponse();
		response.setOperationStatus(0);
		ComputeAccountListnerConfig oldDef = (ComputeAccountListnerConfig) resource;
		try {
			
			ObjStore<ComputeAccountListnerConfig> store = ObjStoreHelper.getStore(ComputeAccountListnerConfig.class);
			String filter = "accountName == '"+oldDef.getAccountName()+"'";
			List<ComputeAccountListnerConfig> list = store.query(filter);
			if (list == null || list.isEmpty()) {
				response.setErrorMessage("No account found with this name:" + oldDef.getAccountName());
				return response;
			}
			store.delete(filter);
			response.setResponse("Deleted successfully");
		} catch (Exception e) {
			response.setOperationStatus(4);
			response.setErrorMessage(e.getMessage());
			logger.info("failed to Delete Compute Resource Account Account", e);
		}
		return response;
    }

    /* (non-Javadoc)
     * @see com.cloupia.service.cIM.tree.MoResourceListener#updateResource(java.lang.Object, java.lang.Object, java.lang.Object)
     */
    @Override
    public ResourceOperationResponse updateResource(MoResourceIf oldResource, MoResourceIf newResource) throws Exception
    {
    	
    	ResourceOperationResponse response = new ResourceOperationResponse();
		response.setOperationStatus(0);
		ComputeAccountListnerConfig oldDef = (ComputeAccountListnerConfig) oldResource;
		ComputeAccountListnerConfig newDef = (ComputeAccountListnerConfig) newResource;
		String accountName = oldDef.getAccountName();
		
		try {
			if(!oldDef.getAccountName().equals(newDef.getAccountName())){
				throw new Exception("Account Names should be same....while doing Update");
			}
			
			ObjStore<ComputeAccountListnerConfig> store = ObjStoreHelper.getStore(ComputeAccountListnerConfig.class);
			String query = "accountName == '"+oldDef.getAccountName()+"'";
			List<ComputeAccountListnerConfig> list = store.query(query);
			if (list == null || list.isEmpty()) {
				response.setErrorMessage("No account found with this name:" + accountName);
				return response;
			}
			store.modifySingleObject(query, newDef);
			response.setResponse("Updated Successfully..");
			
		} catch (Exception e) {
			response.setOperationStatus(4);
			response.setErrorMessage(e.getMessage());
		logger.info("failed to Update Compute Resource Account", e);
		}
		return response;
    }
	

}

