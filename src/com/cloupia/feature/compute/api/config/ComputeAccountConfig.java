package com.cloupia.feature.compute.api.config;

import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.customactions.UserInputField;
import com.cloupia.service.cIM.inframgr.customactions.WorkflowInputFieldTypeDeclaration;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormField;
import com.cloupia.service.cIM.tree.MoReference;

/*
 * XmlRootElement annotation is added to support XML REST APIs.
 * It is mandatory annotaion for REST API payload using root element framework will identify  config.
 * name is mandatory for XmlRootElement annotation.
 * If there is no getter setter method available framework will ignore part of payload.
 */
@XmlRootElement(name = "ComputeAccount")
@PersistenceCapable(detachable = "true", table = "compute_account_config")
public class ComputeAccountConfig implements TaskConfigIf{
	
	public static final String HANDLER_NAME  = "Create Compute Account";
    public static final String HANDLER_LABEL = "Create Compute Account";
      
    @Persistent
    private long               actionId;
    @Persistent
    private long               configEntryId;
	/*
	 * MoReference annotation is used to identify the resource for the URL.
	 * It contains two attributes "path" and "key".
	 * Path attribute is for unique id of resource.It is mandatory. 
	 * key attribute is not mandatory. if key is true framework will identify as primary for the requested URL
	 * 
	 */
	@MoReference(path = "ComputeAccount.ID", key = true)
	@FormField(label = "Account Name", help = "Account Name", mandatory = true)
	@ReportField(label="Account Name")
	@UserInputField(type = WorkflowInputFieldTypeDeclaration.GENERIC_TEXT)
	@Persistent
	private String accountName;
	
	@FormField(label = "Status", help = "Status", mandatory = true)
	@ReportField(label="Status")
	@UserInputField(type = WorkflowInputFieldTypeDeclaration.GENERIC_TEXT)
	@Persistent
 	private String status;
	@FormField(label = "IP Address", help = "IP Address", mandatory = true)
	@ReportField(label="IP Address")
	@Persistent
	@UserInputField(type = WorkflowInputFieldTypeDeclaration.GENERIC_TEXT)
 	private String ip;
	
	
	public ComputeAccountConfig(){}
	
	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	

    public String getDisplayLabel(){
    	return HANDLER_LABEL;
    }
    @XmlTransient
	public long getActionId() {
		return actionId;
	}

	public void setActionId(long actionId) {
		this.actionId = actionId;
	}
	@XmlTransient
	public long getConfigEntryId() {
		return configEntryId;
	}

	public void setConfigEntryId(long configEntryId) {
		this.configEntryId = configEntryId;
	}

	
}
