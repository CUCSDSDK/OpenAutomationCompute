package com.cloupia.feature.compute.api.config;

import javax.jdo.annotations.Column;
import javax.jdo.annotations.NotPersistent;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.xml.bind.annotation.XmlRootElement;

import com.cisco.cuic.api.client.WorkflowInputFieldTypeDeclaration;
import com.cloupia.feature.compute.accounts.forms.UsersForm;
import com.cloupia.feature.compute.accounts.forms.UsersFormValidator;
import com.cloupia.feature.compute.constants.ComputeConstants;
import com.cloupia.lib.easyui.annotations.NonReportField;
import com.cloupia.lib.easyui.annotations.ReportField;
import com.cloupia.model.cIM.FormFieldDefinition;
import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.customactions.UserInputField;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormField;
import com.cloupia.service.cIM.inframgr.forms.wizard.FormManagedList;
import com.cloupia.service.cIM.tree.MoReference;

/*
 * XmlRootElement annotation is added to support XML REST APIs.
 * It is mandatory annotaion for REST API payload using root element framework will identify  config.
 * name is mandatory for XmlRootElement annotation.
 * If there is no getter setter method available framework will ignore part of payload.
 */
@XmlRootElement(name = "ComputeAccountWithUsers")
@PersistenceCapable(detachable = "true", table="compute_create_account_with_users")
public class CreateComputeAccountWithUsersConfig  implements TaskConfigIf{
	

	public static final String HANDLER_NAME  = "Create Compute Account with Users";
    public static final String HANDLER_LABEL = "Create Compute Account with Users";
    
   
    @Persistent
    private long               actionId;
    @Persistent
    private long               configEntryId;
	
	@MoReference(path = "ComputeAccountForm.ID", key = true)
	@FormField(label = "Account Name", help = "Account Name", mandatory = true)
	@ReportField(label="Account Name")
	@Persistent
	@UserInputField(type = WorkflowInputFieldTypeDeclaration.GENERIC_TEXT)
	private String accountName;
	
	@FormField(label = "Status", help = "Status", mandatory = true)
	@ReportField(label="Status")
	@Persistent
	@UserInputField(type = WorkflowInputFieldTypeDeclaration.GENERIC_TEXT)
 	private String status;
	
	@FormField(label = "IP Address", help = "IP Address", mandatory = true)
	@ReportField(label="IP Address")
	@Persistent
	@UserInputField(type = WorkflowInputFieldTypeDeclaration.GENERIC_TEXT)
 	private String ip;
	
	 @Persistent
		@Column(jdbcType = "CLOB")
		@NonReportField
		private String   usersJSONData;

	public String getUsersJSONData() {
		return usersJSONData;
	}

	public void setUsersJSONData(String usersJSONData) {
		this.usersJSONData = usersJSONData;
	}

	@NotPersistent
    @FormField(label = "Users" , help = "Select users", type = FormFieldDefinition.FIELD_TYPE_TABULAR_NO_CHECKBOX, formManagedTable = true, addEntryForm = "default", editEntryForm = "default", deleteEntryForm = "default")
    @NonReportField
    @UserInputField(type = ComputeConstants.USERS_LIST_TYPE)
    private FormManagedList<UsersForm>   usersList    = new FormManagedList<UsersForm>(
                                                                                  UsersForm.class, new UsersFormValidator());


	public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	
	

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}
	

    public String getDisplayLabel(){
    	return HANDLER_LABEL;
    }

	public long getActionId() {
		return actionId;
	}

	public void setActionId(long actionId) {
		this.actionId = actionId;
	}

	public long getConfigEntryId() {
		return configEntryId;
	}

	public void setConfigEntryId(long configEntryId) {
		this.configEntryId = configEntryId;
	}

	public FormManagedList<UsersForm> getUsersList() {
		return usersList;
	}

	public void setUsersList(FormManagedList<UsersForm> usersList) {
		this.usersList = usersList;
	}

}
