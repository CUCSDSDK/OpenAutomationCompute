package com.cloupia.feature.compute.api.config;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.compute.accounts.forms.UsersForm;
import com.cloupia.model.cIM.ReportContext;
import com.cloupia.service.cIM.inframgr.forms.wizard.AbstractObjectUIController;
import com.cloupia.service.cIM.inframgr.forms.wizard.Page;
import com.google.gson.Gson;

public class CreateComputeAccountWithUsersConfigController extends AbstractObjectUIController{

	static final Logger logger = Logger.getLogger(CreateComputeAccountWithUsersConfigController.class);
	@Override
	public void afterBind(Page page, String id, ReportContext context) {
		logger.info("inside CreateComputeAccountWithUsersConfigController.afterBind()");
		super.afterBind(page, id, context);
	}

	@Override
	public void afterMarshall(Page page, String id, ReportContext context, Object pojo) throws Exception {
		logger.info("inside CreateComputeAccountWithUsersConfigController.afterMarshall()");
		super.afterMarshall(page, id, context, pojo);
	}

	@Override
	public void afterUnmarshall(Page page, String id, ReportContext context, Object pojo) throws Exception {
		logger.info("inside CreateComputeAccountWithUsersConfigController.afterUnMarshall()");
		String usersJSONData = null;
		CreateComputeAccountWithUsersConfig config = (CreateComputeAccountWithUsersConfig) pojo;
		List<UsersForm> usersList=config.getUsersList().getList();
		//Commenting the validation for now, to turn this on when we create the enhancement for this API.
		//CSCve20749:ComputeAccountWithUsers API - Users field should be marked as Mandatory
		/*if(usersList == null || usersList.isEmpty()){
			page.setError(id + ".usersList","Users cannot be empty");
			
		}  else {		
			usersJSONData =  new Gson().toJson(usersList);
			config.setUsersJSONData(usersJSONData);
		}*/
		super.afterUnmarshall(page, id, context, pojo);
	}

	@Override
	public void beforeMarshall(Page page, String id, ReportContext context, Object pojo) throws Exception {
		logger.info("inside CreateComputeAccountWithUsersConfigController.beforeMarshall()");
		super.beforeMarshall(page, id, context, pojo);
	}

	@Override
	public void beforeUnmarshall(Page page, String id, ReportContext context, Object pojo) throws Exception {
		logger.info("inside CreateComputeAccountWithUsersConfigController.beforeUnmarshall()");
		super.beforeUnmarshall(page, id, context, pojo);
	}

}
