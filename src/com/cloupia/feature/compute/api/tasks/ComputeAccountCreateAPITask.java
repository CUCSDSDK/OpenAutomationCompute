package com.cloupia.feature.compute.api.tasks;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.compute.api.config.ComputeAccountConfig;
import com.cloupia.feature.compute.api.config.ComputeAccountCreateConfig;
import com.cloupia.feature.compute.constants.ComputeConstants;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.TaskOutputDefinition;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionLogger;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionTriggerContext;

public class ComputeAccountCreateAPITask extends AbstractTask {

	static Logger logger = Logger.getLogger(ComputeAccountCreateAPITask.class);

	@Override
	public void executeCustomAction(CustomActionTriggerContext context, CustomActionLogger actionLogger)
			throws Exception {

		ComputeAccountCreateConfig oldDef = (ComputeAccountCreateConfig) context.loadConfigObject();
		String accountName = oldDef.getAccountName();
		try {
			ObjStore<ComputeAccountConfig> store = ObjStoreHelper.getStore(ComputeAccountConfig.class);
			List<ComputeAccountConfig> list = store.query("accountName == '"+accountName+"'");
			if(list == null || list.isEmpty()){
				ComputeAccountConfig config = new ComputeAccountConfig();
				config.setAccountName(accountName);
				config.setIp(oldDef.getIp());
				config.setStatus(oldDef.getStatus());
				store.insert(config);
			}else{
				throw new Exception("account "+accountName+" already exists.");
			}

			context.saveOutputValue(ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT, oldDef.getAccountName());
			actionLogger.addInfo("ComputeAccount " + oldDef.getAccountName() + " added Successfully");

		} catch (Exception e) {
			actionLogger.addError("failed to Create Compute Account"+e);
			logger.info("failed to Create Compute Account ", e);
			if(!(context.getExternalContext() == CustomActionTriggerContext.REST_CONTEXT))
			context.exit();
			
		}
	}

	@Override
	public TaskConfigIf getTaskConfigImplementation() {
		return new ComputeAccountCreateConfig();
	}

	@Override
	public String getTaskName() {
		return ComputeAccountCreateConfig.HANDLER_LABEL;// DummyAccount.MODIFY_HANDLER_NAME;
	}

	@Override
	public TaskOutputDefinition[] getTaskOutputDefinitions() {
		TaskOutputDefinition[] output = new TaskOutputDefinition[1];
		// NOTE: If you want to use the output of this task as input to another
		// task. Then the second argument
		// of the output definition MUST MATCH the type of UserInputField in the
		// config of the task that will
		// be receiving this output. Take a look at HelloWorldConfig as an
		// example.
		output[0] = new TaskOutputDefinition(ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT,
				ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT, "Account Name");

		return output;
	}

	

}
