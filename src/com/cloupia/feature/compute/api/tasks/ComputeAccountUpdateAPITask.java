package com.cloupia.feature.compute.api.tasks;

import java.util.List;

import org.apache.log4j.Logger;

import com.cloupia.feature.compute.api.config.ComputeAccountConfig;
import com.cloupia.feature.compute.api.config.ComputeAccountUpdateConfig;
import com.cloupia.feature.compute.constants.ComputeConstants;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.TaskOutputDefinition;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionLogger;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionTriggerContext;

public class ComputeAccountUpdateAPITask extends AbstractTask {

	static Logger logger = Logger.getLogger(ComputeAccountUpdateAPITask.class);

	@Override
	public void executeCustomAction(CustomActionTriggerContext context, CustomActionLogger actionLogger)
			throws Exception {
		try {
		ComputeAccountUpdateConfig oldDef = (ComputeAccountUpdateConfig) context.loadConfigObject();
		ObjStore<ComputeAccountConfig> store = ObjStoreHelper.getStore(ComputeAccountConfig.class);
		String accountName = oldDef.getAccountName();

		List<ComputeAccountConfig> list = store.query("accountName == '" + accountName + "'");
		if (list == null || list.isEmpty()) {
			actionLogger.addInfo("No account found with this name:" + accountName);
			throw new Exception("No account found with this name:" + accountName);
		}
			ComputeAccountConfig acc = new ComputeAccountConfig();
			acc.setAccountName(accountName);
			acc.setStatus(oldDef.getStatus());
			acc.setIp(oldDef.getIp());
			String query = "accountName == '" + oldDef.getAccountName() + "'";
			
			store.modifySingleObject(query, acc);
			context.saveOutputValue(ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT, oldDef.getAccountName());
			actionLogger.addInfo("Compute Account " + oldDef.getAccountName() + " modified Successfully");

		} catch (Exception e) {
			actionLogger.addInfo("failed to Update Compute Account");
			logger.error("failed to Update Compute Account", e);
			if(!(context.getExternalContext() == CustomActionTriggerContext.REST_CONTEXT))
				context.exit();
		}
		
	}

	@Override
	public TaskConfigIf getTaskConfigImplementation() {
		return new ComputeAccountUpdateConfig();
	}

	@Override
	public String getTaskName() {
		return ComputeAccountUpdateConfig.MODIFY_HANDLER_NAME;
	}

	@Override
	public TaskOutputDefinition[] getTaskOutputDefinitions() {
		TaskOutputDefinition[] output = new TaskOutputDefinition[1];
		// NOTE: If you want to use the output of this task as input to another
		// task. Then the second argument
		// of the output definition MUST MATCH the type of UserInputField in the
		// config of the task that will
		// be receiving this output. Take a look at HelloWorldConfig as an
		// example.
		output[0] = new TaskOutputDefinition(ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT,
				ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT, "Account Name");
		return output;
	}

}
