package com.cloupia.feature.compute.api.tasks;


import org.apache.log4j.Logger;

import com.cisco.cuic.api.client.WFFieldTypeConstants;
import com.cloupia.feature.compute.api.config.CreateComputeAccountWithUsersConfig;
import com.cloupia.feature.compute.constants.ComputeConstants;
import com.cloupia.fw.objstore.ObjStore;
import com.cloupia.fw.objstore.ObjStoreHelper;
import com.cloupia.service.cIM.inframgr.AbstractTask;
import com.cloupia.service.cIM.inframgr.TaskConfigIf;
import com.cloupia.service.cIM.inframgr.TaskOutputDefinition;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionLogger;
import com.cloupia.service.cIM.inframgr.customactions.CustomActionTriggerContext;

public class ComputeAccountCreateFormManagedTask extends AbstractTask
{

    static Logger logger = Logger.getLogger(ComputeAccountCreateFormManagedTask.class);
    
    @Override
    public void executeCustomAction(CustomActionTriggerContext context, CustomActionLogger actionLogger) throws Exception
    {
    	logger.info("inside ComputeAccountCreateFormManagedTask execute custom action method");
    	CreateComputeAccountWithUsersConfig config = (CreateComputeAccountWithUsersConfig) context.loadConfigObject();
		
		try{
			
			ObjStore<CreateComputeAccountWithUsersConfig> store = ObjStoreHelper.getStore(CreateComputeAccountWithUsersConfig.class);			
			store.insert(config);
			
			context.saveOutputValue(ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT, config.getAccountName());
			context.saveOutputValue(ComputeConstants.COMPUTE_ACCOUNT_IP_OUTPUT, config.getIp());
			context.saveOutputValue(ComputeConstants.COMPUTE_ACCOUNT_STATUS_OUTPUT, config.getStatus());
			actionLogger.addInfo("Compute Account with users "+config.getAccountName()+" added Successfully");
			
		} catch (Exception e) {
			
			logger.info("failed to Create Compute Account with users", e);
		}
           }

    
	@Override
	public TaskConfigIf getTaskConfigImplementation() {
		return new CreateComputeAccountWithUsersConfig();
	}

	@Override
	public String getTaskName() {
		return CreateComputeAccountWithUsersConfig.HANDLER_NAME;//DummyAccount.MODIFY_HANDLER_NAME;
	}

	@Override
	public TaskOutputDefinition[] getTaskOutputDefinitions() {
		TaskOutputDefinition[] output = new TaskOutputDefinition[4];
		//NOTE: If you want to use the output of this task as input to another task. Then the second argument 
		//of the output definition MUST MATCH the type of UserInputField in the config of the task that will
		//be receiving this output.  Take a look at HelloWorldConfig as an example.
		output[0] = new TaskOutputDefinition(
				ComputeConstants.COMPUTE_ACCOUNT_NAME_OUTPUT,
				WFFieldTypeConstants.GENERIC_TEXT,
				"Account Name");
		output[1] = new TaskOutputDefinition(
				"Status",
				WFFieldTypeConstants.GENERIC_TEXT,
				"Status");
		output[2] = new TaskOutputDefinition(
				"ip",
				WFFieldTypeConstants.GENERIC_TEXT,
				"ip");
		output[3] = new TaskOutputDefinition(
				"usersList",
				ComputeConstants.USERS_LIST_TYPE,
				"usersList");
		return output;
	}

}
