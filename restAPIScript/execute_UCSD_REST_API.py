# This script demonstrates how to invoke the CISCO UCSD XML REST API
#
# This script requires Python version 2, release 2.6 or later
# The Python 'requests' HTTP library is used to to invoke the REST API
# CISCO UCSD XML REST API payload and response are in XML format
# The Python 'lxml' XML library is used to construct the XML payload and to parse the XML response
#
# python executable

import sys
import requests
from lxml import etree
import logging

# Use the logging module to log events

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Read the UCSD server IP and REST key from the command line
try:

	number_of_arguments = len(sys.argv)
	if number_of_arguments != 3:
		raise Exception("Invalid number of arguments !! Please run script as 'python script_file_name.py ipAddress UCSD_REST_KEY'")
except Exception as e:
	logger.error("Exception occurred while executing this script : " +str(e))
	sys.exit(1)
# IP address of UCSD server on which the REST API is to be invoked


IP_address = sys.argv[1]

#headers for HTTP requests

headers = {}

# Authenticate the user with the REST key

rest_key_value = sys.argv[2]
content_type = "application/xml"
headers["X-Cloupia-Request-Key"] = rest_key_value
headers["content-type"] = content_type

custom_operation_type = None
resource_URL = None
http_request_type = None
resource_complete_URL = None
response = None
payload_data = None

def main():
 
	#Invoke the HTTP POST REST API operation 'CREATE_COMPUTE_ACCOUNT' to create a ComputeAccount
	#custom_operation_type = CREATE_COMPUTE_ACCOUNT, resource_URL = /cloupia/api-v2/ComputeAccount, HTTP request type = POST

	custom_operation_type = "CREATE_COMPUTE_ACCOUNT"
	resource_URL = "/cloupia/api-v2/ComputeAccount"
	http_request_type = "POST"
	resource_complete_URL = "https://" + IP_address +  resource_URL

	#Construct XML Payload for CREATE_COMPUTE_ACCOUNT

	cuic_operation_request = etree.Element('cuicOperationRequest')
	operation_type = etree.SubElement(cuic_operation_request, 'operationType')
	operation_type.text = custom_operation_type
	payload = etree.SubElement(cuic_operation_request, 'payload')
	compute_account = etree.Element('ComputeAccount')
	account_name = etree.SubElement(compute_account, 'accountName')
	account_name.text = 'sdk_compute'
	status = etree.SubElement(compute_account, 'status')
	status.text = 'On'
	ip = etree.SubElement(compute_account, 'ip')
	ip.text = '1.1.1.1'
	inner_text = etree.tostring(compute_account)
	payload.text = etree.CDATA(inner_text)

	payload_data = etree.tostring(cuic_operation_request)
	
	logger.info("Executing HTTP POST CREATE_COMPUTE_ACCOUNT REST API...")
	logger.info("payload =  %s",payload_data)
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)

	#Invoke the HTTP GET REST API 'Read' operation to read all ComputeAccount
	#resource_URL = /cloupia/api-v2/ComputeAccount, HTTP request type = GET
	
	resource_URL = "/cloupia/api-v2/ComputeAccount"
	http_request_type = "GET"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP GET REST API to read all ComputeAccount resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	#Invoke the HTTP GET REST API 'Read' operation to read a specific ComputeAccount
	#resource_URL = /cloupia/api-v2/ComputeAccount/{accountName}, HTTP request type = GET
	
	resource_URL = "/cloupia/api-v2/ComputeAccount/"+"sdk_compute"
	http_request_type = "GET"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP GET REST API to read a specific ComputeAccount resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	#Invoke the HTTP POST REST API operation 'UPDATE_COMPUTE_ACCOUNT' to update a ComputeAccount
	#custom_operation_type = UPDATE_COMPUTE_ACCOUNT, resource_URL = /cloupia/api-v2/ComputeAccount, HTTP request type = POST

	custom_operation_type = "UPDATE_COMPUTE_ACCOUNT"
	resource_URL = "/cloupia/api-v2/ComputeAccount"
	http_request_type = "POST"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	
	#Construct XML Payload for UPDATE_COMPUTE_ACCOUNT

	cuic_operation_request = etree.Element('cuicOperationRequest')
	operation_type = etree.SubElement(cuic_operation_request, 'operationType')
	operation_type.text = custom_operation_type
	payload = etree.SubElement(cuic_operation_request, 'payload')
	compute_account = etree.Element('ComputeAccount')
	account_name = etree.SubElement(compute_account, 'accountName')
	account_name.text = 'sdk_compute'
	status = etree.SubElement(compute_account, 'status')
	status.text = 'Off'
	ip = etree.SubElement(compute_account, 'ip')
	ip.text = '2.2.2.2'
	inner_text = etree.tostring(compute_account)
	payload.text = etree.CDATA(inner_text)

	payload_data = etree.tostring(cuic_operation_request)
	
	logger.info("Executing HTTP POST UPDATE_COMPUTE_ACCOUNT REST API...")
	logger.info("payload =  %s",payload_data)
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)

	#Verify the ComputeAccount updated with new ip address and status
	#Invoke the HTTP GET REST API 'Read' operation to read a specific ComputeAccount
	#resource_URL = /cloupia/api-v2/ComputeAccount/{accountName}, HTTP request type = GET
	
	resource_URL = "/cloupia/api-v2/ComputeAccount/"+"sdk_compute"
	http_request_type = "GET"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP GET REST API to read a specific ComputeAccount resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	#Invoke the HTTP POST REST API operation 'DELETE_COMPUTE_ACCOUNT' to delete a ComputeAccount
	#custom_operation_type = DELETE_COMPUTE_ACCOUNT, resource_URL = /cloupia/api-v2/ComputeAccount, HTTP request type = POST

	custom_operation_type = "DELETE_COMPUTE_ACCOUNT"
	resource_URL = "/cloupia/api-v2/ComputeAccount"
	http_request_type = "POST"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	
	#Construct XML Payload for DELETE_COMPUTE_ACCOUNT

	cuic_operation_request = etree.Element('cuicOperationRequest')
	operation_type = etree.SubElement(cuic_operation_request, 'operationType')
	operation_type.text = custom_operation_type
	payload = etree.SubElement(cuic_operation_request, 'payload')
	compute_account = etree.Element('ComputeAccount')
	account_name = etree.SubElement(compute_account, 'accountName')
	account_name.text = 'sdk_compute'
	inner_text = etree.tostring(compute_account)
	payload.text = etree.CDATA(inner_text)

	payload_data = etree.tostring(cuic_operation_request)
	
	logger.info("Executing HTTP POST DELETE_COMPUTE_ACCOUNT REST API...")
	logger.info("payload =  %s",payload_data)
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	
	#Verify the deletion of ComputeAccount
	#Invoke the HTTP GET REST API 'Read' operation to read a specific ComputeAccount
	#resource_URL = /cloupia/api-v2/ComputeAccount/{accountName}, HTTP request type = GET
	
	resource_URL = "/cloupia/api-v2/ComputeAccount/"+"sdk_compute"
	http_request_type = "GET"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP GET REST API to read a specific ComputeAccount resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	#Invoke the HTTP POST REST API operation 'CREATE' to create a ComputeResource
	#resource_URL = /cloupia/api-v2/ComputeResource, HTTP request type = POST
	
	resource_URL = "/cloupia/api-v2/ComputeResource"
	http_request_type = "POST"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	
	#Construct XML Payload for ComputeResource@CREATE
	
	cuic_operation_request = etree.Element('cuicOperationRequest')
	payload = etree.SubElement(cuic_operation_request, 'payload')
	compute_resource = etree.Element('ComputeResource')
	account_Name = etree.SubElement(compute_resource, 'accountName')
	account_Name.text = 'sdk_compute'
	status = etree.SubElement(compute_resource, 'status')
	status.text = 'On'
	ip = etree.SubElement(compute_resource, 'ip')
	ip.text = '1.1.1.1'
	inner_text = etree.tostring(compute_resource)
	payload.text = etree.CDATA(inner_text)

	payload_data = etree.tostring(cuic_operation_request)
	
	logger.info("Executing HTTP POST ComputeResource@CREATE REST API...")
	logger.info("payload =  %s",payload_data)
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	#Verify the creation of ComputeResource
	#Invoke the HTTP GET REST API 'Read' operation to read a specific ComputeResource
	#resource_URL = /cloupia/api-v2/ComputeResource/{accountName}, HTTP request type = GET
	
	resource_URL = "/cloupia/api-v2/ComputeResource/"+"sdk_compute"
	http_request_type = "GET"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP GET REST API to read a specific ComputeResource resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	#Invoke the HTTP PUT REST API operation 'UPDATE' to update a ComputeResource
	#resource_URL = /cloupia/api-v2/ComputeResource/{accountName}, HTTP request type = UPDATE
	
	resource_URL = "/cloupia/api-v2/ComputeResource/"+"sdk_compute"
	http_request_type = "PUT"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	
	#Construct XML Payload for ComputeResource@UPDATE

	cuic_operation_request = etree.Element('cuicOperationRequest')
	payload = etree.SubElement(cuic_operation_request, 'payload')
	compute_resource = etree.Element('ComputeResource')
	account_name = etree.SubElement(compute_resource, 'accountName')
	account_name.text = 'sdk_compute'
	status = etree.SubElement(compute_resource, 'status')
	status.text = 'Off'
	ip = etree.SubElement(compute_resource, 'ip')
	ip.text = '2.2.2.2'
	inner_text = etree.tostring(compute_resource)
	payload.text = etree.CDATA(inner_text)

	payload_data = etree.tostring(cuic_operation_request)
	
	logger.info("Executing HTTP UPDATE ComputeResource@UPDATE REST API...")
	logger.info("payload =  %s",payload_data)
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	
	#Verify ComputeResource account updated with new ip address and status
	#Invoke the HTTP GET REST API 'Read' operation to read a specific ComputeResource
	#resource_URL = /cloupia/api-v2/ComputeResource/{accountName}, HTTP request type = GET
	
	resource_URL = "/cloupia/api-v2/ComputeResource/"+"sdk_compute"
	http_request_type = "GET"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP GET REST API to read a specific ComputeResource resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	
	#Invoke the HTTP DELETE REST API Read operation - DELETE a specific ComputeResource
	#resource_URL = /cloupia/api-v2/ComputeResource/{accountName}, HTTP request type = DELETE
	
	resource_URL = "/cloupia/api-v2/ComputeResource/"+"sdk_compute"
	http_request_type = "DELETE"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP DELETE REST API to delete a specific ComputeResource resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	

	#Verify the deletion of ComputeResource 'sdk_compute' 
	#Invoke the HTTP GET REST API 'Read' operation to read a specific ComputeResource
	#resource_URL = /cloupia/api-v2/ComputeResource/{accountName}, HTTP request type = GET
	
	resource_URL = "/cloupia/api-v2/ComputeResource/"+"sdk_compute"
	http_request_type = "GET"
	resource_complete_URL = "https://" + IP_address +  resource_URL
	payload_data = None
	response = None
	logger.info("Executing HTTP GET REST API to read a specific ComputeResource resource ...")
	response = invoke_rest_API(resource_complete_URL, payload_data,  http_request_type)
	logger.info("API Response HTTP Status Code =  %s", response.status_code);
	logger.info("Response content type =  %s", response.headers['content-type'])
	logger.info("Response content =  %s", response._content)
	
	


	
	

def invoke_rest_API(complete_URL, payload_data, http_request_type):
	requests.packages.urllib3.disable_warnings()
	response = None
	if http_request_type == "GET":
		response = requests.get(complete_URL, headers=headers, verify=False)
	elif http_request_type == "POST":
		response = requests.post(complete_URL, data=payload_data, headers=headers, verify=False)
	elif http_request_type == "PUT":
		response = requests.put(complete_URL, data=payload_data, headers=headers, verify=False)
	elif http_request_type == "DELETE":
		response = requests.delete(complete_URL, headers=headers, verify=False)
	else:
		raise Exception("Invalid HTTP request type")
	return response

if __name__ == "__main__":
	try:
		main ()
	except Exception as e:
		logger.error("Exception occurred while executing this script : " +str(e))
		


